import Vue from 'vue'
import Router from 'vue-router'
// import Home from './views/Home.vue'
// import NavHeader from './views/NavHeader.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Reg',
      name: 'Reg',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './components/RegPage.vue')
  },
  {
    path: '/Home',
    name: 'Home',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './components/Home.vue')
  }
  ]
})
